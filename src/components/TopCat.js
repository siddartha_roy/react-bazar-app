import { Card, CardActionArea, CardMedia, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import Carousel from 'react-elastic-carousel'
import WidgetsRoundedIcon from '@mui/icons-material/WidgetsRounded';

import watch from './watch.webp'

const breakpoints=[
    {width:1,itemsToShow:1},
    {width:550,itemsToShow:2},
    {width:768,itemsToShow:3},
    {width:1200,itemsToShow:3},
]

const items=[
    {
        id:'1',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-1.png&w=1920&q=75'
    },
    {
        id:'2',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-2.png&w=1920&q=75'
    },
    {
        id:'3',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-3.png&w=1920&q=75'
    },
    {
        id:'4',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-1.png&w=1920&q=75'
    }
]

function TopCat(){
    return(
      <Box sx={{
        display:'flex',
        flexDirection:'column',
        backgroundColor: 'primary.light',
        height:'100%',
        alignItems:'center',
       
      }}>
        <Box sx={{
            width:'85%',
            textAlign:'left',
            paddingTop: '10px'

        }}>
             <Typography variant='h4' fontWeight='bold'><WidgetsRoundedIcon fontSize="large"
                        sx={
                            {color: 'red'}
                        }/>Top Categories</Typography>

        </Box>
        <Box sx={{
            width:'85%',
            paddingTop:'20px'
        }}>
        <Carousel breakPoints={breakpoints}>
            {items.map(item=>
                 <Box sx={{
                    
                    width:'85px',
                    display:'flex',
                    width:400
                    
                 }}>
                    <Card   key={item.id} sx={{
                                height:'100px',
                                padding:'10px',
                                display:'flex',
                                justifyContent:'space-evenly',
                                borderRadius:'10px',minWidth:300, maxWidth: 500 }}>
                        <CardActionArea>
                        <CardMedia
                        component="img"
                        height="100"
                        borderRadius="2px"
                       
                        image={item.img}
                        alt={item.id}
                        />
                        </CardActionArea>

                        
                    </Card>
                 </Box>
                
                )}
        </Carousel>
        </Box>

       </Box>
       
     
    )
}

export default TopCat