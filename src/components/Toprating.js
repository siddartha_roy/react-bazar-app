import {
    ButtonBase,
    Card,
    CardActionArea,
    CardContent,
    CardHeader,
    Grid,
    ListItem,
    Stack,
    Typography
} from "@mui/material";
import {Box, height, margin, padding} from "@mui/system";
import React from "react";
import CardMedia from '@mui/material/CardMedia';
import StarPurple500Icon from '@mui/icons-material/StarPurple500';
import Rating from "@mui/material/Rating";


const items = [
    {
        id: '1',
        title:'Camera',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fcamera-1.png&w=1920&q=75'
    }, {
        id: '2',
        title:'Shoe',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fshoes-2.png&w=1920&q=75'
    }, {
        id: '3',
        title:'phone',
        
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fmobile-1.png&w=1920&q=75'

    }, {
        id: '4',
        title:'watch',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fwatch-1.png&w=1920&q=75'
    }
]

const products=[
    {
        id:'1',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flondon-britches.png&w=1920&q=75',
        title:'London Britches'
    },
    {
        id:'2',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fjim%20and%20jiko.png&w=1920&q=75',
        title:'London Britches'
    }
]


function Toprating() {
    return (

        <Box sx={
            {
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: 'primary.light',
                height: '100%',
                alignItems: 'center'

            }
        }>
            <Box sx={
                {width: '85%'}
            }>
                <Grid container
                    spacing={3}>
                    <Grid item
                        xs={12}
                        xl={6}>
                        <Box sx={
                            {
                                display: 'flex',
                                width: '85%',
                                textAlign: 'left',
                                paddingTop: '10px',
                                marginTop:'20px'
                                

                            }
                        }>
                            <Typography variant='h4' fontWeight='bold'><StarPurple500Icon fontSize="large"
                                    sx={
                                        {color: 'orange'}
                                    }/>Top Ratings</Typography>

                        </Box>
                        <Grid container sx={{
                            backgroundColor:'white',
                             padding:'10px',
                             marginTop:'20px',
                             height:'250px',

                             borderRadius:'10px'}}
                            spacing={2}>
                            {
                            items.map(item => <Grid item
                                xs={6} xl={3} sx={{
                                    fontFamily:"inherit",
                                    fontSize:'14px',
                                    fontWeight:'600'
                                }}>
                                <Card sx={
                                    {
                                        display: 'flex',
                                        flexDirection: 'column',
                                        borderRadius: '0.5rem',
                                        boxShadow: '0',
                                        height:'100%',
                                        
                                    }
                                }>
                                    <CardActionArea>
                                        <CardMedia component="img" height="100" borderRadius="2px"

                                            image={
                                                item.img
                                            }
                                            alt={
                                                item.id
                                            }/>
                                    </CardActionArea>
                                    <CardContent>
                                    <Rating name="read-only" value={3} readOnly />
                                    <Typography variant="span">{item.title}</Typography>
                                    <Typography variant="body1" color="error">$180.00  </Typography>

                                    </CardContent>

                                </Card>
                            </Grid>)
                        } </Grid>
                    </Grid>

                    <Grid item
                        xs={12}
                        xl={6}>
                        <Box sx={
                            {
                                display: 'flex',
                                width: '85%',
                                textAlign: 'left',
                                paddingTop: '10px',
                                marginTop:'20px'
                                

                            }
                        }>
                            <Typography variant='h4' fontWeight='bold'><StarPurple500Icon fontSize="large"
                                    sx={
                                        {color: 'orange'}
                                    }/>Feautred Brands</Typography>

                        </Box>
                        <Grid container sx={{
                            backgroundColor:'white',
                             padding:'10px',
                             marginTop:'20px',
                             height:'250px',

                             borderRadius:'10px'}}
                            spacing={2}>
                            {
                            products.map(item => <Grid item
                                xs={12} xl={6} sx={{
                                    fontFamily:"inherit",
                                    fontSize:'14px',
                                    fontWeight:'600'
                                }}>
                                <Card sx={
                                    {
                                        display: 'flex',
                                        flexDirection: 'column',
                                        borderRadius: '0.5rem',
                                        boxShadow: '0',
                                        
                                    }
                                }>
                                    <CardActionArea>
                                        <CardMedia component="img" height='150' orderRadius="2px"

                                            image={
                                                item.img
                                            }
                                            alt={
                                                item.id
                                            }/>
                                    </CardActionArea>
                                    <CardContent>
                                    
                                    <Typography variant="span">{item.title}</Typography>
                                    

                                    </CardContent>

                                </Card>
                            </Grid>)
                        } </Grid>
                    </Grid>


                    


                </Grid>

            </Box>

        </Box>

    )
}
export default Toprating
