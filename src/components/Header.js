import React from "react";
import { useState } from "react";
import Stack from "@mui/material/Stack";
import {Box} from "@mui/system";
import {Button, Container, Link, ListItem, TextField} from "@mui/material";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import Badge from "@mui/material/Badge";
import logo2 from './logo2.svg'
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import InputAdornment from "@mui/material/InputAdornment";
import ButtonBase from "@mui/material/ButtonBase";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormControl from '@mui/material/FormControl';
import  './search-css.css'



function Header() {

    const [categoryValue,setCategory]=useState('All Categories');

   
    return (
        <Box sx={{
            zIndex:2,
            position:'relative',
            boxShadow:'none',
           
        }}>
            <Box sx={{
                zIndex:3,
                position:'relative',
                height:'80px',
                transition:'height 250ms ease-in-out',
                background:'#fff',
                borderBottom:'1px solid #ddd',
                position:'static'
            }}>
                <Container sx={{
                    display:"flex",
                    justifyContent:'space-between',
                    gap:"16px",
                    alignItems:'center'
                    
                }}>
                    <Box sx={{
                        display:"flex",
                        marginRight:'16px',
                        alignItems:'center',
                        minWidth:'170px'

                    }}>
                        <Link href='/'>
                            <img alt="logo" src="https://bazar-react.vercel.app/assets/images/logo2.svg"/>
                        </Link>

                    </Box>

                    <Box sx={{
                        display:'flex',
                        justifyContent:"center",
                        flex:"1 1 0",
                        paddingTop:'10px'
                    }}>
                        <Box sx={{
                            position:'relative',
                            flex:'1 1 0',
                            marginLeft:'auto',
                            marginRight:'auto',
                            maxWidth:'670px'

                        }}>
                            <FormControl sx={{
                width: '100%'
              }}>
                <TextField
                className="inputRounded"
                  placeholder="Searching for..."
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchOutlinedIcon />
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position="end">
                        <Select label="All Categories" value={categoryValue} onChange={(e)=>setCategory(e.target.value)} sx={{
                            backgroundColor:'#F6F9FC',
                            color:'#4B566B',
                            marginRight:'-14px',
                            borderTopRightRadius:'300px',
                            borderTopLeftRadius:'0px, !important',
                            borderBottomRightRadius:'300px',
                            borderBottomLeftRadius: '0px !important',
                            borderLeft: '1px solid #DAE1E7 !important',
                            padding:'8px'
                            
                           
                            

                        }}>
                            <MenuItem value='All Categories'>All Categories</MenuItem>
                            <MenuItem value='Car'>Car</MenuItem>
                            <MenuItem value='Clothes'>Clothes</MenuItem>
                            <MenuItem value='Electronics'>Electronics</MenuItem>
                            <MenuItem value='Laptop'>Laptop</MenuItem>
                            <MenuItem value='Toys'>Toys</MenuItem>

                        </Select>
                      </InputAdornment>
                    ),
                  }}
                  
                 />
              </FormControl>

                        </Box>

                    </Box>

                    <Box sx={{
                        display:'flex',
                        alignItems:'center'
                    }}>
                        <ButtonBase sx={{
                padding: '10px',
                backgroundColor: '#F3F5F9',
                color: 'rgba(0, 0, 0, 0.54)',
                borderRadius: '50%',
                textAlign: 'center',
                flex: '0 0 auto',
                overflow: 'visible',
                alignItems: 'center'
            }}>
              <PersonOutlinedIcon />
            </ButtonBase>
            <Badge>
              <ButtonBase sx={{
                marginLeft: '20px',
                padding: '10px',
                backgroundColor: '#F3F5F9',
                borderRadius: '50%',
                color: 'rgba(0, 0, 0, 0.54)'
              }}>
                <Badge badgeContent={3} color="error">
                  <ShoppingBagOutlinedIcon />
                </Badge>
              </ButtonBase>
            </Badge>

                    </Box>

                </Container>

            </Box>

        </Box>
       
    )
}

export default Header
