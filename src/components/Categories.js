import React from "react";
import {Box} from "@mui/system";
import watch from './watch.webp'
import {
    Button,
    ButtonBase,
    Grid,
    ListItem,
    Stack,
    Typography
} from "@mui/material";
import WidgetsRoundedIcon from '@mui/icons-material/WidgetsRounded';

const items = [
    {
        id: '1',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F7.DenimClassicBlueJeans.png&w=64&q=75',
        title: 'AutoMobile'
    },
    {
        id: '2',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F20.GrayOvercoatWomen.png&w=64&q=75',
        title: 'Cars'
    },
    {
        id: '3',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F8.IndianPearlThreadEarrings.png&w=64&q=75',
        title: 'Fashion'
    },
    {
        id: '4',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F21.FeathersandBeadsBohemianNecklace.png&w=64&q=75',
        title: 'Mobile'
    }, {
        id: '5',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FShoes%2F11.Flowwhite.png&w=64&q=75',
        title: 'Laptop'
    }, {
        id: '6',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F1.SaktiSambarPowder.png&w=64&q=75',
        title: 'Desktop'
    }, {
        id: '1',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F7.DenimClassicBlueJeans.png&w=64&q=75',
        title: 'AutoMobile'
    }, {
        id: '2',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F20.GrayOvercoatWomen.png&w=64&q=75',
        title: 'Cars'
    }, {
        id: '3',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F8.IndianPearlThreadEarrings.png&w=64&q=75',
        title: 'Fashion'
    }, {
        id: '4',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F21.FeathersandBeadsBohemianNecklace.png&w=64&q=75',
        title: 'Mobile'
    }, {
        id: '5',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FShoes%2F11.Flowwhite.png&w=64&q=75',
        title: 'Laptop'
    }, {
        id: '6',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F1.SaktiSambarPowder.png&w=64&q=75',
        title: 'Desktop'
    }

]

function Categories() {
    return (


        <Box sx={
            {
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: 'primary.light',
                alignItems: 'center',
                height: '100%'


            }
        }>
            <Box sx={
                {
                    width: '85%',
                    textAlign: 'left',
                    paddingTop: '50px'

                }
            }>
                <Typography variant='h4' fontWeight='bold'><WidgetsRoundedIcon fontSize="large"
                        sx={
                            {color: 'red'}
                        }/>Categories</Typography>

            </Box>

            <Box sx={
                {
                    width: '85%',
                    textAlign: 'left',
                    paddingTop: '30px',
                    height: '100%'
                }
            }>

                <Grid container
                    spacing={2}>

                    {
                    items.map((item) => {
                        return (
                            <Grid item
                                lg={2}
                                md={4}
                                sm={6}
                                xs={12}
                                key={
                                    item.id
                            }>
                                <ButtonBase>
                                    <Stack variant="outlined"
                                        spacing={2}
                                        direction="row"
                                        elevation={4}
                                        sx={
                                            {
                                                backgroundColor: 'white',
                                                alignItems: 'center',
                                                padding: "10px",
                                                borderRadius: '20px'

                                            }
                                    }>
                                        <Box>
                                            <img src={
                                                    item.img
                                                }
                                                alt={
                                                    item.title
                                                }/>
                                        </Box>
                                        <Box>
                                            <Typography>{
                                                item.title
                                            }</Typography>
                                        </Box>
                                    </Stack>
                                </ButtonBase>

                            </Grid>
                        )
                    })
                } </Grid>
            </Box>

        </Box>


    )
}

export default Categories
