import {
    Box,
    Grid,
    Link,
    ListItem,
    Typography
} from "@mui/material";
import {height} from "@mui/system";
import React from "react";
import Stack from "@mui/material/Stack";

function Footer() {

    return (
        <Box sx={
            {
                backgroundColor: "secondary.main",
                height: "100%",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                padding: '30px',
                marginTop: '20px'
            }
        }>
            <Box sx={
                {
                    width: "80%",
                    textAlign: "left",
                    paddingTop: "50px"
                }
            }>
                <Grid container
                    sx={
                        {color: "white"}
                }>
                    <Grid item
                        lg={3}
                        sm={6}
                        xs={12}
                        sx={
                            {padding: "5px"}
                    }>
                        <Link href="#">
                            <img src="https://bazar-react.vercel.app/assets/images/logo.svg" alt="logo"/>
                        </Link>
                        <Typography variant="body2"
                            sx={
                                {
                                    color: 'secondary.dark',
                                    
                                    marginTop: '15px',
                                    whiteSpace: 'normal',
                                    fontSize: '16px',
                                    padding: '5px'
                                }
                        }>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor
                                          libero id et, in gravida. Sit diam duis mauris nulla cursus. Erat
                                          et lectus vel ut sollicitudin elit at amet
                        </Typography>
                    </Grid>

                    <Grid item
                        lg={3}
                        sm={6}
                        xs={12}
                        sx={
                            {padding: "5px"}
                    }>
                        <Typography variant="h5"
                            sx={
                                {
                                    marginBottom: '20px',
                                    
                                    fontWeight: '600'
                                }
                        }>About Us</Typography>
                        <Stack direction="column"
                            spacing={2}
                            sx={
                                {
                                    color:'secondary.dark',
                                    
                                }
                        }>
                            <Link href="#" color="inherit" underline="none">
                                Careers
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Our Stores
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Our Cares
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Terms & Conditions
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Privacy Policy
                            </Link>
                        </Stack>
                    </Grid>

                    <Grid item
                        lg={3}
                        sm={6}
                        xs={12}
                        sx={
                            {padding: "5px"}
                    }>
                        <Typography variant="h5"
                            sx={
                                {
                                    marginBottom: '20px',
                                    
                                    fontWeight: '600'
                                }
                        }>Customer Care</Typography>
                        <Stack direction="column"
                            spacing={2}
                            sx={
                                {
                                    color: 'secondary.dark',
                                    
                                }
                        }>
                            <Link href="#" color="inherit" underline="none">
                                Help Center
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                How to Buy
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Track Your Order
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Corporate & Bulk Purchasing
                            </Link>
                            <Link href="#" color="inherit" underline="none">
                                Returns & Refunds
                            </Link>
                        </Stack>
                    </Grid>

                    <Grid item
                        lg={3}
                        sm={6}
                        xs={12}
                        sx={
                            {padding: "5px"}
                    }>
                        <Typography variant="h5"
                            sx={
                                {
                                    marginBottom: '20px',
                                    
                                    fontWeight: '600'
                                }
                        }>Contact Us</Typography>
                        <Stack direction="column"
                            spacing={2}
                            sx={
                                {
                                    color: 'secondary.dark',
                                   
                                }
                        }>
                            <Typography variant="body2">
                                70 Washington Square South, New York, NY 10012, United States
                            </Typography>
                            <Typography variant="body2">
                                Email: uilib.help@gmail.com
                            </Typography>
                            <Typography variant="body2">Phone: +1 1123 456 780</Typography>
                        </Stack>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    )
}
export default Footer
