import  Container  from "@mui/system/Container";
import React from "react";
import Carousel from 'react-elastic-carousel'
import Stack  from "@mui/material/Stack";
import { Box } from "@mui/system";
import { Button, Typography } from "@mui/material";

const items = [
    {
        id: '1',
        img: 'https://bazar-react.vercel.app/assets/images/products/nike-black.png',
        title: '50% off For Your First Shopping',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
    },
    {
        id: '2',
        img: 'https://bazar-react.vercel.app/assets/images/products/nike-black.png',
        title: '50% off For Your First Shopping',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
    }
]



function Banner(){
    return(
        <Container>
            <Box sx={{marginTop:'40px', marginBottom:'60px'}}>
            <Carousel>
                {items.map(item=>
                  <Stack spacing={4} direction="row" alignItems='center' justifyContent='center'>
                    <Box sx={{
                        textAlign:'left',
                        paddingTop:'50px'
                        }}>
                        <Typography variant="h2" fontWeight="bold">{item.title}</Typography>
                        <Typography variant="body2" color='rgba(0, 0, 0, 0.54)'>{item.subtitle}</Typography>
                        <Button variant="contained" sx={{
                            backgroundColor:'#D23F57',
                            marginTop:'25px'
                        }}>shop Now</Button>
                    </Box>
                    <Box>
                        <img src={item.img} alt={item.title} width='500px'/>
                    </Box>
                  </Stack>

                )}

                
            </Carousel>
            </Box>


        </Container>
    )
}

export default Banner