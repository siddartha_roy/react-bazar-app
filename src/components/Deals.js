import React from "react";
import Carousel from 'react-elastic-carousel'
import Card  from "@mui/material/Card";
import CardHeader from '@mui/material/CardHeader';
import Avatar from "@mui/material/Avatar";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";

import logo2 from './logo2.svg'
import flash from './flash.webp'
import watch from './watch.webp'
import mobile from './mobile.webp'
import swatch from './swatch.webp'
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, Stack } from '@mui/material';

import Rating from "@mui/material/Rating";
import { red } from "@mui/material/colors";
import  Chip  from "@mui/material/Chip";
import BoltOutlinedIcon from '@mui/icons-material/BoltOutlined';
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import AddBoxOutlinedIcon from '@mui/icons-material/AddBoxOutlined';

const breakpoints=[
    {width:1,itemsToShow:1},
    {width:550,itemsToShow:2},
    {width:768,itemsToShow:3},
    {width:1200,itemsToShow:4},
]

const items=[
    {id:1,tiltle:"one",image:flash,discount:"25% off"},
    {id:2,tiltle:"two",image:watch,discount:"30% off"},
    {id:3,tiltle:"three",image:mobile,discount:"28% off"},
    {id:4,tiltle:"four",image:swatch,discount:"35% off"},
    {id:5,tiltle:"five",image:flash,discount:"40% off"},
    {id:6,tiltle:"six",image:mobile,discount:"25% off"}
]

function Deals(){

    return(
        <div>
            <Box sx={{
                display:'flex',
                flexDirection:'column',
                backgroundColor: 'primary.light',
                alignItems: 'center',
                height: '100%'
                
                
            }}>
            <Box sx={{
                width: '90%',
                textAlign: 'left',
                paddingTop: '50px',
                paddingBottom:'20px'
                

            }}>
                <Typography variant='h4' fontWeight='bold' sx={{ color: 'secondary.light', fontFamily: 'fontFamily' }}><BoltOutlinedIcon fontSize="large" sx={{color: 'red'}}/>Flash Deals</Typography>
            </Box>
            
           <Carousel breakPoints={breakpoints}>
            {items.map(item=>
                <Card variant="outlined" elevation={5} key={item.id} sx={{width:"300px", maxWidth: 400 }}>
                <CardHeader
                  avatar={
                    <Chip color="error"  label={item.discount} />
                  }
                  action={<FavoriteBorderOutlinedIcon/>}
                />
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="200"
                    image={item.image}
                    alt={item.tiltle}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      {item.tiltle}
                    </Typography>
                    <Rating name="read-only" value={3} readOnly />
                    <Box sx={{
                      display:"flex",
                      justifyContent:"space-between",
                      alignItems:"center"
                    }}>
                      <Box sx={{
                        display:"flex",

                      }}>
                    <Typography variant="body1" color="error">$180.00  </Typography>
                    <Typography variant="body1" marginLeft='5px' >$220</Typography>
                    </Box>

                    <Box>
                      <Button sx={{padding:'3px'}}>
                      <AddBoxOutlinedIcon color="error" sx={{fontSize:'27px'}}/>
                      </Button>
                    </Box>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
                )}
            
            
            
           </Carousel>
           </Box>
           
        </div>
    )
}

export default Deals