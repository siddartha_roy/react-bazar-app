import {
    ButtonBase,
    Card,
    CardActionArea,
    CardContent,
    CardHeader,
    Grid,
    ListItem,
    Stack,
    Typography
} from "@mui/material";
import {Box, height, margin, padding} from "@mui/system";
import React from "react";
import CardMedia from '@mui/material/CardMedia';
import FiberNewRoundedIcon from '@mui/icons-material/FiberNewRounded';


const items = [
    {
        id: '1',
        title:'Camera',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fcamera-1.png&w=1920&q=75'
    }, {
        id: '2',
        title:'Shoe',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fshoes-2.png&w=1920&q=75'
    }, {
        id: '3',
        title:'phone',
        
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fmobile-1.png&w=1920&q=75'

    }, {
        id: '4',
        title:'watch',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fwatch-1.png&w=1920&q=75'
    }
]

const products=[
    {
        id:'1',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flondon-britches.png&w=1920&q=75',
        title:'London Britches'
    },
    {
        id:'2',
        img:'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fjim%20and%20jiko.png&w=1920&q=75',
        title:'London Britches'
    }
]


function Arrivals() {
    return (

        <Box sx={
            {
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: 'primary.light',
                height: '100%',
                alignItems: 'center'

            }
        }>
            <Box sx={
                {width: '85%'}
            }>
                <Grid container
                    spacing={3}>
                    <Grid item
                        xl={12}
                        >
                        <Box sx={
                            {
                                display: 'flex',
                                width: '85%',
                                textAlign: 'left',
                                paddingTop: '10px',
                                marginTop:'20px'
                                

                            }
                        }>
                            <Typography variant='h4' fontWeight='bold'><FiberNewRoundedIcon fontSize="large"
                                    sx={
                                        {color: 'green'}
                                    }/>New Arrivals</Typography>

                        </Box>
                        <Grid container sx={{
                            backgroundColor:'white',
                             padding:'10px',
                             marginTop:'20px',
                             height:'250px',

                             borderRadius:'10px'}}
                            spacing={2}>
                            {
                            items.map(item => <Grid item
                                xs={6} xl={3} spacing={2} sx={{
                                    fontFamily:"inherit",
                                    fontSize:'14px',
                                    fontWeight:'600'
                                }}>
                                <Card sx={
                                    {
                                        display: 'flex',
                                        flexDirection: 'column',
                                        borderRadius: '0.5rem',
                                        boxShadow: '0',
                                        height:'100%',
                                        
                                    }
                                }>
                                    <CardActionArea>
                                        
                                        <img  height="130px" src={item.img}/>
                                        
                                       {/*} <CardMedia component="img" width="100" height="150px" borderRadius="2px"

                                            image={
                                                item.img
                                            }
                                            alt={
                                                item.id
                                            }/>*/}
                                    </CardActionArea>
                                    <CardContent>
                                    
                                    <Typography variant="span">{item.title}</Typography>
                                    <Typography variant="body1" color="error">$180.00  </Typography>

                                    </CardContent>

                                </Card>
                            </Grid>)
                        } </Grid>
                    </Grid>

                    


                    


                </Grid>

            </Box>

        </Box>

    )
}
export default Arrivals
