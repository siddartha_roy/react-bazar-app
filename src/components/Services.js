import React from "react";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import CreditScoreOutlinedIcon from "@mui/icons-material/CreditScoreOutlined";
import GppGoodOutlinedIcon from "@mui/icons-material/GppGoodOutlined";
import SupportAgentOutlinedIcon from "@mui/icons-material/SupportAgentOutlined";
import { Box } from "@mui/system";
import { Button, Card, CardContent, Grid } from "@mui/material";
import {Typography} from "@mui/material";

const services = [
    {
      icon: LocalShippingOutlinedIcon,
      title: "Worldwide Delivery",
      subtitle:
        "We offer competitive prices on our 100 million plus product any range.",
    },
    {
      icon: CreditScoreOutlinedIcon,
      title: "Safe Payment",
      subtitle:
        "We offer competitive prices on our 100 million plus product any range.",
    },
    {
      icon: GppGoodOutlinedIcon,
      title: "Shop With Confidence",
      subtitle:
        "We offer competitive prices on our 100 million plus product any range.",
    },
    {
      icon: SupportAgentOutlinedIcon,
      title: "24/7 Support",
      subtitle:
        "We offer competitive prices on our 100 million plus product any range.",
    },
  ];

function Services(){
    return(
        <Box sx={{
            display:'flex',
            flexDirection:"column",
            backgroundColor:'#F6F9FC',
            alignItems:'center',
            height:"100%",
            marginBottom:'50px'
        }}>
            <Box sx={{
                width:"85%",
                textAlign:'left',
                paddingTop:'50px',
                height:'100%'
            }}>

                <Grid container spacing={4}>
                    {services.map(service=>
                           <Grid item lg={3} sm={6} xs={12}>
                            <Card sx={{
                                height:'200px',
                                padding:'30px',
                                borderRadius:'10px'
                            }}>
                                <CardContent sx={{textAlign:'center'}}>
                                    <Button sx={{
                                        margin:'0 auto',
                                        padding:'15px',
                                        backgroundColor: '#F3F5F9',
                                        color: 'rgba(0, 0, 0, 0.54)',
                                        borderRadius: '50%'

                                    }}>
                                        <service.icon sx={{fontSize:'30px'}}/>

                                    </Button>
                                    <Typography variant="h6" sx={{
                                        marginTop:'15px',
                                        marginBottom:'10px',
                                        color:'secondary.light',
                                        fontFamily:'fontFamily',
                                        fontSize:'18px'
                                        
                                    }}>
                                        {service.title}

                                    </Typography>

                                    <Typography variant="body2" sx={{
                                             color: '#7D879C',
                                              fontFamily: 'fontFamily'
                                         }}>
                                        {service.subtitle}
                                    </Typography>

                                </CardContent>

                            </Card>
                           </Grid>

                        )}
                </Grid>

            </Box>
           

        </Box>
        
    )
}

export default Services