import React from "react";
import {Box} from "@mui/system";
import {Grid, Typography} from "@mui/material";
import {Card} from "@mui/material";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import CardHeader from "@mui/material/CardHeader";
import CardActionArea from "@mui/material/CardActionArea";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import AddBoxOutlinedIcon from '@mui/icons-material/AddBoxOutlined';
import Chip from "@mui/material/Chip";
import CardMedia from "@mui/material/CardMedia";
import Rating from "@mui/material/Rating";
import Button from "@mui/material/Button";

const items = [
    {
        id: '1',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F21.TarzT3.png&w=1920&q=75',
        title: 'Tarz T3'
    },
    {
        id: '2',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F22.YamahaR15Black.png&w=1920&q=75',
        title: 'Yamaha R15 Black'
    },
    {
        id: '3',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F23.YamahaR15Blue.png&w=1920&q=75',
        title: 'Yamaha R15 Blue'
    },
    {
        id: '4',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F24.Revel2020.png&w=1920&q=75',
        title: 'Xevel 2020'
    }, {
        id: '5',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F25.JacksonTB1.png&w=1920&q=75',
        title: 'Jackson TB1'
    }, {
        id: '6',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F1.Siri2020.png&w=1920&q=75',
        title: 'Siri 2020'
    }, {
        id: '7',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F2.COSOR1.png&w=1920&q=75',
        title: 'Cosor1'
    }, {
        id: '8',
        img: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75',
        title: 'Tarz T3'
    }
]

function More() {

    return (
        <Box sx={
            {
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: 'primary.light',
                alignItems: 'center',
                height: '100%'

            }
        }>
            <Box sx={
                {
                    width: '85%',
                    textAlign: 'left',
                    paddingTop: '50px'

                }
            }>
                <Typography variant='h4' fontWeight='bold' sx={{color:'secondar.light',fontFamily:'fontFamily'}}>More For You</Typography>

            </Box>
            <Box sx={
                {
                    width: '85%',
                    textAlign: 'left',
                    paddingTop: '30px',
                    height: '100%'
                }
            }>
                <Grid container
                    spacing={1}>
                    {
                    items.map(item => {
                        return (
                            <Grid item lg={3} sm={6} xs={12}
                                >
                                <Card variant="outlined"
                                    elevation={5}
                                    key={
                                        item.id
                                    }
                                    sx={
                                        {
                                            width: "300px",
                                            maxWidth: 400
                                        }
                                }>
                                    <CardHeader avatar={
                                            <Chip
                                        color="error"
                                        label="25% off"/>
                                        }
                                        action={<FavoriteBorderOutlinedIcon/>}/>
                                    <CardActionArea>
                                        <CardMedia component="img" height="200"
                                            image={
                                                item.img
                                            }
                                            alt={
                                                item.tiltle
                                            }/>
                                        <CardContent>
                                            <Typography gutterBottom variant="h6" component="div">
                                                {
                                                item.tiltle
                                            } </Typography>
                                            <Rating name="read-only"
                                                value={3}
                                                readOnly/>
                                            <Box sx={
                                                {
                                                    display: "flex",
                                                    justifyContent: "space-between",
                                                    alignItems: "center"
                                                }
                                            }>
                                                <Box sx={
                                                    {display: "flex"}
                                                }>
                                                    <Typography variant="body1" color="error">$180.00
                                                    </Typography>
                                                    <Typography variant="body1" marginLeft='5px'>$220</Typography>
                                                </Box>

                                                <Box>
                                                    <Button sx={
                                                        {padding: '3px'}
                                                    }>
                                                        <AddBoxOutlinedIcon color="error"
                                                            sx={
                                                                {fontSize: '27px'}
                                                            }/>
                                                    </Button>
                                                </Box>
                                            </Box>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>


                            </Grid>
                        )
                    })
                } </Grid>

            </Box>

        </Box>

    )
}
export default More
