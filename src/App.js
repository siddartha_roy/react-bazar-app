import React from "react";
import Header from "./components/Header";
import { Button } from "@mui/material";
import Deals from "./components/Deals";
import Banner from "./components/Banner";
import Toprating from "./components/Toprating";
import TopCat from "./components/TopCat";
import Categories from "./components/Categories";
import More from "./components/More";
import Footer from "./components/Footer";
import Services from "./components/Services";
import { createTheme,ThemeProvider } from "@mui/material";
import Arrivals from "./components/Arrivals";

const theme=createTheme({
  palette:{
    primary:{
      main:'#0F3460',
      light:'#F6F9FC'
    },
    secondary:{
      main:'#0c0e30',
      light:'2B3445',
      dark:'#AEB4BE'
    }
  },
  typography: {
    fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
    
  }

})


const App = () => {
  return (
    <ThemeProvider theme={theme}>
    <div>
      <Header/>
      <Banner/>
      <Deals/>
      <TopCat/>
      <Toprating/>
      <Arrivals/>
   
      <Categories/>
      <More/>
      <Services/>
  <Footer/> 
  
      
      
      
    </div>
    </ThemeProvider>
  );
};

export default App;